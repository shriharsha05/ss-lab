%{
#include<stdio.h>
#include<stdlib.h>
int count = 0;
%}

%token ID NUMBER NL S IF RELOP

%%
stmt : ifstmnt NL {printf("\n If count : %d",count); exit(0);}
     ;

ifstmnt : IF'('condition')''{'ifstmnt'}' {count++;}
	|S
	;

condition : x RELOP x
	  | condition  
	;

x : ID | NUMBER
;

%%

main()
{
printf("Enter expression : ");
yyparse();
}

int yywrap()
{
return 1;
}

int yyerror(char *msg)
{
printf("Invalid");
}

	
