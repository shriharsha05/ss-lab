#include<stdio.h>
#include<ctype.h>
#include<string.h>

int main()
{
	int i=0;
	char str[33];
	char key[][10]={"int","char","float","double","if","else","do","for","switch","class"};
	printf("Enter a string : ");
	gets(str);
	for( ; i<10;i++)
	{
		if(strcmp(key[i],str)==0)
		{
			printf("\nYou entered a keyword!");
			return 0;
		}
	}

	if(strlen(str)>31)
	{
		printf("\nInvalid Identifier(Length exceeded!)");
		return 0;
	}

	for(i=0;str[i]!='\0';i++)
	{
		if(str[i]==' ')
		{
			printf("\nInvalid Identifier(Spaces are not allowed!)");
			return 0;
		}
	}

	if(!((str[0]=='_')||(isalpha(str[0]))))
	{
		printf("\nInvalid Identifier(Should begin with letter or underscore)");
		return 0;
	}

	for(i=1;i<strlen(str);i++)
		{
			if(!((isalnum(str[i]))||(str[i]=='_')))
			{
				printf("\nInvalid Identifier(Special Characters are not allowed)");
				return 0;
			}

		}
	printf("\nValid Identifier!");
	return 0;
}