%{
#include<stdio.h>
int sf=0,pf=0;
%}

%%
"scanf" {sf++;fprintf(yyout,"readf");}
"printf" {pf++;fprintf(yyout,"writef");}
%%

int yywrap()
{
return 1;
}

int main()
{
yyin=fopen("sample.c","r");
yyout=fopen("output.txt","w");
yylex();
fclose(yyout);
system("cat output.txt");
printf("\nNo of Scanf Encountered : %d\nNo of Printf Encountered : %d",sf,pf);
return 0;
}

