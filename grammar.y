%{
#include<stdio.h>
#include<stdlib.h>
%}

%token A B

%%
stmt : expr '\n' {printf("\nValid Expression"); exit(0);}
     ;

expr : A expr B
     |
     ;
%%

main()
{
printf("Enter expression : ");
yyparse();
}

int yywrap()
{
return 1;
}

int yyerror(char *msg)
{
printf("Invalid");
}
	
