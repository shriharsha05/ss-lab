%{
#include"y.tab.h"
%}

%%
\n return NL;
"if" return IF;
"<"|"<="|">"|">="|"=="|"!=" return RELOP;
[sS][0-9]* return S;
[0-9]+ return NUMBER;
[a-zA-Z][a-zA-Z0-9_]* return ID;
[ \t]+ ;
. return yytext[0];
%%
