%{
#include<stdio.h>
#include<stdlib.h>
%}

%token ID NUMBER NL

%%
stmt : expr NL {printf("\nValid Expression"); exit(0);}
     ;

expr : expr '+' expr
     | expr '-' expr
     | expr '*' expr
     | expr '/' expr
     | '('expr')'
     |ID
     |NUMBER;
%%

main()
{
printf("Enter expression : ");
yyparse();
}

int yywrap()
{
return 1;
}

int yyerror(char *msg)
{
printf("Invalid");
}

	
