%{
#include"y.tab.h"
%}

%%
[0-9]+ return NUMBER;
[a-zA-Z][a-zA-Z0-9_]* return ID;
\n return NL;
[ \t]+ ;
. return yytext[0];
%%
