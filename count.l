%{
#include<stdio.h>
int word=0,chr=0,nline=0,spce=0;
%}

%%
[\n] {nline++;chr++;}
[ ]+ {spce+=yyleng;}
[^ \n\t]+ {word++;chr+=yyleng;}
. {chr++;}
%%

int main(int argc,char **argv)
{
	yyin = fopen(argv[1],"r");
	yylex();
	printf("\nWords : %d\nCharacters : %d\nNew Line : %d\n Space = %d",word,chr,nline,spce);
	return 0;
}

int yywrap()
{
return 1;
}
