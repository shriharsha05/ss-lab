%{
#include<stdio.h>
int vowel = 0;
int cons = 0;
%}

%%
[aeiouAEIOU] {vowel++;}
[a-zA-Z] {cons++;}
%%

void main()
{
	printf("\nEnter the string :");
	yylex();
	printf("\nNo of Vowels : %d\nNo of Consonants : %d",vowel,cons);
}

int yywrap()
{
	return 1;
}
